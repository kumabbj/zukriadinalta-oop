<?php

require('Animal.php');
require('Ape.php');
require('Frog.php');

$sheep = new Animal("Shaun");

echo "Nama Hewan : $sheep->name <br>"; // "shaun"
echo "Jumlah Kaki : $sheep->legs <br>"; // 4
echo "Hewan Berdarah Dingin : $sheep->cold_blooded <br>"; // "no"


$sungokong = new Ape("Kera Sakti");

echo "<br> Nama Hewan : $sungokong->name <br>"; 
echo "Jumlah Kaki : $sungokong->legs <br>"; 
echo "Hewan Berdarah Dingin : $sungokong->cold_blooded <br>"; 
$sungokong->yell(); // "Auooo"

$kodok  = new Frog("Buduk");

echo "<br> <br> Nama Hewan : $kodok->name <br>"; 
echo "Jumlah Kaki : $kodok->legs <br>"; 
echo "Hewan Berdarah Dingin : $kodok->cold_blooded <br>"; 
$kodok ->yell(); // "Auooo"

?>